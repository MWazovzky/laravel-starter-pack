# Laravel Starter Pack

Quikly set up new laravel 8 project with
[laravel/breaze](https://laravel.com/docs/8.x/starter-kits#laravel-breeze),
[laravel/sanctum](https://laravel.com/docs/8.x/sanctum),
and
[vue-3](https://v3.vuejs.org/)
runnung inside docker container.

## Installation guide

### Clone project

```
git clone git@bitbucket.org:MWazovzky/laravel-starter-pack.git
cd laravel-starter-pack
```

### Create `.env` file

```
cp .env.example .env
```

### Create `docker-compose.override.yml` to run mysql in container locally

```
cp docker-compose.override.yml.example docker-compose.override.yml
```

### Start docker

```
docker-compose up -d
```

### Install php dependencies

```
docker exec app composer install
```

### Generate app key

```
docker exec app php artisan key:generate
```

### Run migrations

```
docker exec app php artisan migrate
```

### Run tests to make sure your app is up and running

```
docker exec app php artisan test
```

### Install node dependencies

```
npm install && npm run dev
```

Application should now be available at `http://localhost`
