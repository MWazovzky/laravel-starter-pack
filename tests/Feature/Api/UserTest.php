<?php

namespace Tests\Feature\Api;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function testGet()
    {
        $user = User::factory()->create([
            'name' => 'John',
            'email' => 'john@example.com',
        ]);

        Sanctum::actingAs($user, ['*']);

        $response = $this->json('GET', route('api.user.show'));

        $response->assertStatus(200);
        $response->assertJson([
            'id' => $user->id,
            'name' => 'John',
            'email' => 'john@example.com',
        ]);
    }

    public function testGetAnauthenticated()
    {
        $response = $this->json('GET', route('api.user.show'));

        $response->assertStatus(401);
    }
}
