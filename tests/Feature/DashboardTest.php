<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DashboardTest extends TestCase
{
    use RefreshDatabase;

    public function testDashboard()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get(route('dashboard'));

        $response->assertStatus(200);
    }

    public function testDashboardAnauthenticated()
    {
        $response = $this->get(route('dashboard'));

        $response->assertStatus(302);

        $this->assertEquals(route('login'), $response->getTargetUrl());
    }
}
